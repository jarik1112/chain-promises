# README #

### What is this repository for? ###

* This is simple function to chain promise execution and get list of results from them
* Version 0.0.4

### How to setup ###

```
npm i https://jarik1112@bitbucket.org/jarik1112/chain-promises.git
```

### How to use ###

```javascript
const chainPromises = require('chain-promises');
// Pass list of functions which should return new promise
chainPromises(
  [
    promiseTimeout(),
    promiseTimeout(),
  ]
).then((result) => {
  /// ... your code
})
```

Passing second param as ```true``` will reject queue on first reject

```javascript
const chainPromises = require('chain-promises');
// Pass list of functions which should return new promise
chainPromises(
  [
    promiseTimeout(),
    promiseTimeout(),
  ],
  // If true function will work as Promise.all
  true /// <--- on false function will work as Promise.allSettled
).then((result) => {
  /// ... your code
})
```


Latest API you can find in [test](https://bitbucket.org/jarik1112/chain-promises/src/develop/test/index.spec.ts)

### Owner ###

Yaroslav Parchsvkiy <jarik1112@yahoo.com>
