import { chainPromises } from '../src';

function promiseTimeout(start?: number, timeout = 100): () => Promise<number> {
  return () =>
    new Promise<number>((done) =>
      setTimeout(() => done(start ? Date.now() - start : Date.now()), timeout),
    );
}

describe('Run chain test', () => {
  describe('Resolve with all resolved promises', () => {
    it('Should resolve', () => {
      expect(
        chainPromises<number>([promiseTimeout(), promiseTimeout()]),
      ).resolves.toBeInstanceOf(Array);
    });

    it('Should run promises in queue', (done) => {
      const start = Date.now();
      chainPromises<number>([promiseTimeout(start), promiseTimeout(start)]).then((result) => {
        expect(result).toHaveLength(2);
        const [first, second] = result as PromiseFulfilledResult<number>[];
        expect(first).toHaveProperty('status', 'fulfilled');
        expect(first).toHaveProperty('value');

        expect(first.value).toBeGreaterThanOrEqual(90);

        expect(second).toHaveProperty('status', 'fulfilled');
        expect(second).toHaveProperty('value');

        expect(second.value).toBeGreaterThanOrEqual(200);

        done();
      });
    });
  });

  describe('Resolve with one reject promise', () => {
    it('Should resolve even if on promise fails', () => {
      expect(
        chainPromises<number>([
          promiseTimeout(),
          promiseTimeout(),
          () => new Promise((_, reject) => setTimeout(() => reject('Error'), 100)),
        ]),
      ).resolves.toBeInstanceOf(Array);
    });

    it('Should has one rejected result', (done) => {
      const start = Date.now();
      chainPromises<number>([
        () => new Promise((_, reject) => setTimeout(() => reject('Error'), 100)),
        promiseTimeout(start),
        promiseTimeout(start),
      ]).then((result) => {
        expect(result).toHaveLength(3);
        const [first, second] = result as PromiseFulfilledResult<number>[];
        expect(second).toHaveProperty('status', 'fulfilled');
        expect(second).toHaveProperty('value');
        expect(second.value).toBeGreaterThanOrEqual(190);

        expect(first).toHaveProperty('status', 'rejected');
        expect(first).toHaveProperty('reason', 'Error');
        done();
      });
    });
  });

  describe('Check reject', () => {
    it('Should fail on first reject', () => {
      expect(
        chainPromises<number>(
          [
            promiseTimeout(),
            promiseTimeout(),
            () => new Promise((_, reject) => setTimeout(() => reject('Error'), 100)),
          ],
          true,
        ),
      ).rejects.toEqual('Error');
    });

    it('Should not fail', () => {
      expect(
        chainPromises<number>([promiseTimeout(), promiseTimeout()], true),
      ).resolves.toBeInstanceOf(Array);
    });
  });
});
