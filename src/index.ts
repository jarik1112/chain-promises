/**
 *
 * @param list Array of functions returns promise
 * @param rejectAllOnReject Boolean if true will reject promises on first reject
 * otherwise will act as Promise.allSettled
 * @returns
 */

export async function chainPromises<T>(
  list: Array<() => Promise<T>>,
  rejectAllOnReject: boolean = false,
): Promise<PromiseSettledResult<T>[]> {
  return new Promise((mainDone, mainReject) => {
    mainDone(
      list.reduce((prev, item) => {
        return prev.then((results) => {
          return new Promise((done) => {
            item().then(
              (value) => {
                results.push({
                  status: 'fulfilled',
                  value,
                });
                done(results);
              },
              (reason) => {
                if (rejectAllOnReject) {
                  return mainReject(reason);
                }
                results.push({
                  status: 'rejected',
                  reason,
                });
                done(results);
              },
            );
          });
        });
      }, Promise.resolve([] as PromiseSettledResult<T>[])),
    );
  });
}
